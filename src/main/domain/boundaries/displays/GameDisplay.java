package main.domain.boundaries.displays;

import main.domain.core.models.Archetype;
import main.domain.core.models.Card;

import java.util.List;

public interface GameDisplay {
    void set(List<Archetype> archetypes, List<Card> cardsPlayed, List<Card> cardsLeft);
    void reset();
    void resetCurrentDeck();
}
