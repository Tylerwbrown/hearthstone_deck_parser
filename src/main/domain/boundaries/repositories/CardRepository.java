package main.domain.boundaries.repositories;

import main.domain.core.models.Card;

public interface CardRepository {
    Card getOne(String nameOrID);
}
