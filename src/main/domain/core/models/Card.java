package main.domain.core.models;

public class Card {
    // NOTE: Potential data: cardSet, type, rarity, flavor, goldIMG
    public final String name;
    public final String id;
    public final int cost;
    public final String description;
    public final String playerClass;
    public final String imgURL;

    public Card (
        String name, String id, int cost,
        String description, String playerClass, String imgURL
    ) {
        this.name = name;
        this.id = id;
        this.cost = cost;
        this.description = description;
        this.playerClass = playerClass;
        this.imgURL = imgURL;
    }
}
