package main.domain.core.models;

import java.util.List;

public class Archetype {
    public final List<Deck> decks;
    public final int count;
    public final String name;
    public final String playerClass;

    public Archetype(List<Deck> decksOfSameClass) {
        this.decks = decksOfSameClass;
        var sample_deck = decks.get(0);

        this.count = decks.size();
        this.name = sample_deck.name;
        this.playerClass = sample_deck.playerClass;
    }
}
