package main.domain.core.interactors;

import main.domain.core.models.Archetype;
import main.domain.core.models.Card;

import java.util.List;

public class ArchetypeHasDecksPredicate {
    public static boolean perform(Archetype archetype, List<Card> cards) {
        return archetype.decks.stream()
            .anyMatch(deck -> DeckHasEveryCardPredicate.performWithCards(deck, cards));
    }
}
