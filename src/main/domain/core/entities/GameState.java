package main.domain.core.entities;

import main.domain.core.models.Archetype;
import main.domain.core.models.Card;
import main.domain.core.models.Deck;
import main.domain.boundaries.displays.GameDisplay;
import main.domain.core.interactors.ArchetypeHasDecksPredicate;
import main.domain.core.interactors.DeckHasEveryCardPredicate;
import main.domain.boundaries.repositories.GameInputRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class GameState {
    public List<Archetype> archetypes = new ArrayList<>();
    public List<Card> cardsPlayed = new ArrayList<>();
    public List<Card> cardsLeft = new ArrayList<>();

    private GameDisplay gameDisplay;

    public void start(GameDisplay gameDisplay, GameInputRepository gameInputRepository) {
        this.gameDisplay = gameDisplay;
        gameDisplay.reset();

        gameInputRepository.addCardListener(this::addCard);
        gameInputRepository.addEndGameProcedure(gameDisplay::reset);
        gameInputRepository.startListening();
    }

    private void addCard(Card card) {
        cardsPlayed.add(card);

        if(cardsLeft.contains(card))
            cardsLeft.remove(card);
        else // TODO: Run calculation for deck existence (in interactor)
            gameDisplay.resetCurrentDeck();

        removeArchetypesWithoutDecks();

        System.out.println("Running!" + archetypes.size());
        gameDisplay.set(archetypes, cardsPlayed, cardsLeft);
    }

    private void removeArchetypesWithoutDecks() {
        archetypes = archetypes.stream()
            .filter(archetype -> ArchetypeHasDecksPredicate.perform(archetype, cardsPlayed))
            .collect(Collectors.toList());
    }

    private void removeDecksMissingCards() {
        archetypes.forEach(archetype ->
            archetype.decks.removeAll(
                archetype.decks.stream()
                    .filter(deck -> DeckHasEveryCardPredicate.performWithCards(deck, cardsPlayed)) // TODO: This might be backwards
                    .collect(Collectors.toList())
            )
        );
    }

    private List<Deck> decksWithoutCards() {

    }
}
