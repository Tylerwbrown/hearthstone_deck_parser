package main.data.repositories;

import main.data.wrappers.LogReader;
import main.domain.core.models.Card;
import main.domain.boundaries.repositories.CardRepository;
import main.domain.boundaries.repositories.GameInputRepository;
import tools.Procedure;
import tools.U;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class PowerLogListener implements GameInputRepository {
    private static String hearthstonePowerLogPath;
    private static List<Consumer<Card>> cardConsumers = new ArrayList<>();
    private static List<Procedure> endGameProcedures = new ArrayList<>();
    private static CardRepository cardRepository;

    public PowerLogListener(CardRepository cardRepo, String hearthstoneDIR) {
        cardRepository = cardRepo;
        hearthstonePowerLogPath = hearthstoneDIR;
    }

    public void addCardListener(Consumer<Card> cardListener) { cardConsumers.add(cardListener); }
    public void addEndGameProcedure(Procedure endGameProcedure) { endGameProcedures.add(endGameProcedure); }

    public void startListening() {
        new LogReader(hearthstonePowerLogPath).startWithLineOperation(line -> {
            if(cardPlayed(line))
                cardConsumers.forEach(consumer -> consumer.accept(getCard(line)));
            else if(gameEnded(line))
                endGameProcedures.forEach(Procedure::run);
        });
    }

    private static Card getCard(String line) {
        var id = line.substring(line.indexOf("CardID=") + 7);
        return cardRepository.getOne(id);
    }

    private static boolean gameEnded(String line) {
        var endGameRequirements = List.of("TAG_CHANGE", "Entity=GameEntity", "tag=STEP", "value=FINAL_GAMEOVER");
        return U.allStringsExist(line, endGameRequirements);
    }

    private static boolean cardPlayed(String line) {
        var opponentCardRequirements = List.of(
            "SHOW_ENTITY", "Updating Entity=", "entityName=UNKNOWN ENTITY",
            "[cardType=INVALID]", "zone=HAND", "GameState.DebugPrintPower()"
        );

        var coin = "GAME_005";
        var omissions = List.of(coin);
        return U.allStringsExist(line, opponentCardRequirements) && !U.anyStringsExist(line, omissions);
    }
}
