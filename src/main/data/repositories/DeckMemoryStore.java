package main.data.repositories;

import main.data.repositories.boundaries.gateways.DeckGateway;
import main.domain.core.models.Deck;
import main.domain.boundaries.repositories.CardRepository;
import main.domain.boundaries.repositories.DeckRepository;

import java.util.*;

public class DeckMemoryStore implements DeckRepository {
    private Map<String, List<Deck>> cardNameToDecks = new HashMap<>();
    private Map<Integer, Deck> idToDeck = new HashMap<>();

    public DeckMemoryStore(DeckGateway deckGateway, CardRepository cardRepository) {
        deckGateway.createAll(cardRepository).forEach(this::addDeck);
    }

    public List<Deck> getAll() { return new ArrayList<>(idToDeck.values()); }
    public List<Deck> getAllByCardName(String cardName) { return cardNameToDecks.get(cardName); }
    public Deck getOne(int deckID) { return idToDeck.get(deckID); }

    private void addDeck(Deck deck) {
        idToDeck.put(deck.getId(), deck);

        deck.getCardNames().forEach(name -> {
            var decks = cardNameToDecks.getOrDefault(name, new ArrayList<>());
            decks.add(deck);
            cardNameToDecks.put(name, decks);
        });
    }


}
