package main.data.repositories.boundaries.gateways;

import main.domain.core.models.Card;

import java.util.List;

public interface CardGateway {
    List<Card> createAll();
}
