package main.data.repositories.boundaries.gateways;

import main.domain.core.models.Deck;
import main.domain.boundaries.repositories.CardRepository;

import java.util.List;

public interface DeckGateway {
    List<Deck> createAll(CardRepository cardRepository);
}
