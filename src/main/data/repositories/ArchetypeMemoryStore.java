package main.data.repositories;

import main.domain.core.models.Archetype;
import main.domain.boundaries.repositories.ArchetypeRepository;
import main.domain.boundaries.repositories.DeckRepository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ArchetypeMemoryStore implements ArchetypeRepository {
    private Map<String, Archetype> nameToArchetype = new HashMap<>();

    public ArchetypeMemoryStore(DeckRepository deckRepo) {
        deckRepo.getAll().stream()
            .collect(Collectors.groupingBy(deck -> deck.name))
            .entrySet().stream().map(entry -> new Archetype(entry.getValue()))
            .forEach(archetype -> nameToArchetype.put(archetype.name, archetype));
    }

    public List<Archetype> getAll() {
        return new ArrayList<>(nameToArchetype.values());
    }
}
