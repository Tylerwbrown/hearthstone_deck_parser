package main.data.wrappers;

import com.mashape.unirest.http.Unirest;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Map;

public class BaseAPI extends Unirest {
    public static JSONObject getObject(String url) { return getObject(url, Map.of()); }

    public static JSONObject getObject(String url, Map<String, String> headers) {
        var arr = getArray(url, headers);
        return (JSONObject) arr.get(arr.length()-1); // Final version is (in my sample) always the right version.
    }

    public static JSONArray getArray(String url, Map<String, String> headers) {
        try {
            var request = Unirest.get(url);
            headers.forEach(request::header);
            var node = request.asJson().getBody();
            return node.getArray();
        } catch(Exception e) {
            System.out.println("Exception: BaseAPI#getArray");
            return null;
        }
    }

    public static void post(String url, Map<String, String> headers) {
        try {
            BaseAPI.post(url).headers(headers);
        } catch(Exception e) {
            System.out.println("EXCEPTION: BaseAPI#post");
        }
    }
}
