package tools;

public interface Procedure {
    void run();
}
