package tools;


import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class U {
    public static void times(Procedure procedure, int x) { IntStream.range(0, x).forEach(i -> procedure.run()); }

    public static boolean anyStringsExist(String str, List<String> requirements) {
        return requirements.stream().filter(str::contains).collect(Collectors.toList()).size() > 0;
    }

    public static boolean allStringsExist(String str, List<String> requirements) {
        return requirements.stream().filter(str::contains).collect(Collectors.toList()).size() == requirements.size();
    }

    public static Stream<JSONObject> jsonArrayStreamOfJsonObjects(JSONArray arr) {
        return StreamSupport.stream(arr.spliterator(), false).map(obj -> (JSONObject) obj);
    }
}

